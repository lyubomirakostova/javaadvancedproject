package com.company;

public class MethodExecutor {
    static HelloWorld templateClass = new HelloWorld();

    public static void main() {
        int a = 7;
        System.out.println(templateClass.sum(a));
    }
}
