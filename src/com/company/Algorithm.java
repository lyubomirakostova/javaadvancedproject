package com.company;

import com.company.operations.Operation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

public class Algorithm {

    public static void start() throws IOException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Scanner scanner = new Scanner(System.in);
        start:
        while (true) {
            String line = scanner.nextLine();
            String[] input = line.split(" ");


            if (input[0].equals("test") && line.contains("{")) {
                TestCompiler.main("@Test\n" +
                        "public  void " + line.substring(5));
                line = scanner.nextLine();
                while (!(line.contains("}"))) {
                    TestCompiler.main(line);
                    line = scanner.nextLine();

                    if (line.contains("assert")) {
                        line = line.replace(",", "==");
                    }
                }
                TestCompiler.main("}");

                Compiler.compileClass(Tests.class);
            } else if (input[0].equals("test") && !(line.contains("{"))) {

                Method method = Tests.class.getMethod(input[1].replace("(", "").replace(")", "").replace(";", ""));
                Tests tests = new Tests();
                method.invoke(tests);

            } else if (!line.contains("{") && line.contains("(") && line.contains(")")) {
                String[] parameters = line.substring(line.lastIndexOf("(") + 1, line.lastIndexOf(")")).trim().split(" ");
                String params = "";
                for (int i = 0; i < parameters.length - 1; i++) {
                    System.out.println(parameters[i]);
                    //      params += "int " + parameters[i] + " = " + variables.get(new Variable(parameters[i].substring(0, parameters[i].length() - 1))) + ";";
                    params += "int " + parameters[i] + " = " + Operation.getVariable(Operation.initializeVariable(parameters[i].substring(0, parameters[i].length() - 1)), false, false) + ";";
                    System.out.println(params);

                }
                params += "int " + parameters[parameters.length - 1].substring(0, parameters.length - 1) + " = " + Operation.getVariable(Operation.initializeVariable(parameters[parameters.length - 1]/*.substring(0, parameters[parameters.length-1].length() - 1))*/), false, false) + ";";
                System.out.println(params);


                InlineCompiler.execute(params + "System.out.println(templateClass." + line + ");");
                Method method = MethodExecutor.class.getDeclaredMethod("main");
                method.invoke(MethodExecutor.class);
            } else if ("int".equals(input[0]) && !(Arrays.asList(input).contains("{"))) {
                if (input.length >= 5 && input[2].equals("=")) {
                    // for example: int c = a + b;
                    Variable variable = Operation.initializeVariable(input[1]);
                    input[input.length - 1] = input[input.length - 1].substring(0, input[input.length - 1].length() - 1);
                    String[] newArray = new String[input.length - 3];
                    System.arraycopy(input, 3, newArray, 0, newArray.length);
                    try {
                        Operation.putVariable(variable, Operation.calculate(newArray));
                    } catch (IllegalArgumentException | NullPointerException e) {
                        System.out.println(e.getMessage());
                    }

                } else if (input.length >= 3 && input[2].equals("=")) {
                    // for example: int a = 5;
                    Variable variable = new Variable("test");
                    try {
                        variable = Operation.initializeVariable(input[1]);
                        Operation.putVariable(variable, Integer.parseInt(input[3].substring(0, (input[3].length() - 1))));
                    } catch (NumberFormatException ex) {
                        // if it is not a number
                        int variableValue = Operation.getVariable(new Variable(input[3].substring(0, input[3].length() - 1)), false, true);
                        Operation.putVariable(variable, variableValue);

                    }

                } else {
                    // for example: int a;
                    Operation.initializeVariable(input[1].substring(0, (input[1].length() - 1)));
                }
            } else if ((input.length > 1 && "+".equals(input[1])) || (input.length > 1 && "-".equals(input[1]))) {
                // for example: a + b
                try {
                    System.out.println(Operation.calculate(input));
                } catch (IllegalArgumentException | NullPointerException e) {
                    System.out.println(e.getMessage());
                }
            } else if ("stop".equals(input[0])) {
                break start;

                // default will be used for assigning a value to an existing variable
            } else if (Arrays.asList(input).contains("{")) {

                InlineCompiler.main("public static " + line);
                line = scanner.nextLine();
                input = line.split(" ");
                while (!(Arrays.asList(input).contains("}"))) {
                    InlineCompiler.main(line);
                    line = scanner.nextLine();
                    input = line.split(" ");
                }
                InlineCompiler.main("}");


                Compiler.compileClass(HelloWorld.class);
                Compiler.compileClass(MethodExecutor.class);
            } else {
                if (input.length > 3) {
                    // for example: c = a + b;
                 //   Variable variable = Operation.initializeVariable(input[0]);
                   Operation.getVariable(new Variable(input[0]),false,false);
                    input[input.length - 1] = input[input.length - 1].substring(0, input[input.length - 1].length() - 1);
                    String[] newArray = new String[input.length - 2];
                    System.arraycopy(input, 2, newArray, 0, newArray.length);

                    Operation.putVariable(new Variable(input[0]), Operation.calculate(newArray));
                } else if (input.length == 1) {
                    // for example: a
                    int variableValue;
                    try {
                        variableValue = Operation.getVariable(new Variable(input[0]), true, true);

                    } catch (ExceptionInInitializerError ex) {
                        System.out.println(ex.getMessage());
                        continue;
                    }
                    System.out.println(variableValue);
                } else {

                    // for example: a = 5;
                    Variable variable = new Variable("test");
                    try {
                        variable = new Variable(input[0]);
                        Operation.putVariable(variable, Integer.parseInt(input[2].substring(0, input[2].length() - 1)));
                    } catch (NumberFormatException ex) {
                        int variableValue = Operation.getVariable(new Variable(input[2].substring(0, input[2].length() - 1)), false, false);
                        Operation.putVariable(variable, variableValue);
                    }
                }
            }
        }
    }

}
