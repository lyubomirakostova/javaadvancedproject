package com.company;

import java.util.Objects;

public class Variable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return Objects.equals(this.name, variable.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
