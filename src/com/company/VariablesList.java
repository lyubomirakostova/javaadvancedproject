package com.company;

import java.util.HashMap;
import java.util.Map;

public class VariablesList {
    Map<Variable, Integer> variables;

    public VariablesList() {
        this.variables = new HashMap<>();
    }

    public void put(Variable variable, Integer number) {
        variables.put(variable, number);
    }

    public int get(Variable variable) {
        Integer result = variables.get(variable);
        if (!variables.containsKey(variable)) {
            System.out.println("There is no such variable in the program yet!");
        } else if (variables.keySet().stream().noneMatch(variable1 -> variable1.getName().equals(variable.getName()))) {
            throw new NullPointerException(String.format("%s is not declared%n", variable.getName()));
        } else if (result == null) {
            //  throw new IllegalArgumentException(String.format())
            throw new IllegalArgumentException(String.format("%s is not initialized%n", variable.getName()));
        }
        return result;
    }
}
