package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;

public class InlineCompiler {

    public static void main(String string) throws IOException, NoSuchFieldException {
        StringBuilder sb = new StringBuilder(64);

        File helloWorldJava = new File("src/com/company/HelloWorld.java");
        Path path = helloWorldJava.toPath();
        sb.append(Files.readAllLines(path).toString().replace(",", ""));

        System.out.println(path);
        sb.insert(sb.length() - 2, string);
        sb.deleteCharAt(0);
        sb.deleteCharAt(sb.length() - 1);
        if (helloWorldJava.getParentFile().exists() || helloWorldJava.getParentFile().mkdirs()) {

            try {
                Writer writer = null;
                try {
                    writer = new FileWriter(helloWorldJava);
                    writer.write(sb.toString());
                    writer.flush();
                } finally {
                    try {
                        writer.close();
                    } catch (Exception e) {
                    }
                }


            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }
    }


    public static void execute(String string) throws IOException, NoSuchFieldException {
        StringBuilder sb = new StringBuilder(64);
        sb.append("package com.company;\n");
        sb.append("public class MethodExecutor {\n");
        sb.append("static HelloWorld templateClass=new HelloWorld();\n");
        sb.append(" public static void main() {\n");
        sb.append("}\n");
        sb.append("}\n");

        File helloWorldJava = new File("src/com/company/MethodExecutor.java");
        sb.insert(sb.length() - 5, string);
        if (helloWorldJava.getParentFile().exists() || helloWorldJava.getParentFile().mkdirs()) {

            try {
                Writer writer = null;
                try {
                    writer = new FileWriter(helloWorldJava);
                    writer.write(sb.toString());
                    writer.flush();
                } finally {
                    try {
                        writer.close();
                    } catch (Exception e) {
                    }
                }
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }
    }
}
