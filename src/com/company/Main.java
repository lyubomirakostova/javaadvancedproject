package com.company;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Algorithm.start();
    }
}



