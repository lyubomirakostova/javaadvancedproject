package com.company.operations;

import com.company.Variable;
import com.company.VariablesList;

public abstract class Operation {
    public static VariablesList variables = new VariablesList();

    public static Variable initializeVariable(String input) {
        Variable variable = new Variable(input);
        variables.put(variable, null);
        return variable;
    }

    public static int getVariable(Variable variable, boolean NullPointer, boolean IllegalArgument) {
        try {
            return variables.get(variable);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new ExceptionInInitializerError(e.getMessage());
        }
    }


    public static void putVariable(Variable variable,
                                   int value) {
        try {
            variables.get(variable);

        } catch (NullPointerException ex) {
            return;
        } catch (IllegalArgumentException e) {
            variables.put(variable, value);
            return;
        }
        variables.put(variable, value);


    }


    public static int calculate(String[] array) {
        int result = 0;
        Variable variable;
        for (int i = 0; i < array.length; i += 2) {
            int num;
            try {
                // used to calculate '5 + 3', int a = 3 + c;, etc. -> numbers are allowed
                num = Integer.parseInt(array[i]);
                if (i == 0) {
                    result = num;
                    continue;
                }
                if (array[i - 1].equals("+")) {
                    result = result + num;
                }
                if (array[i - 1].equals("-")) {
                    result = result - num;
                }

            } catch (NumberFormatException ex) {
                // used to calculate c = a + b;, etc.

                variable = new Variable(array[i]);
                if (i == 0) {
                    result = variables.get(variable);
                    continue;
                }
                if (array[i - 1].equals("+")) {
                    result = result + variables.get(variable);
                }
                if (array[i - 1].equals("-")) {
                    result = result - variables.get(variable);
                }
            }
        }
        return result;
    }
}
